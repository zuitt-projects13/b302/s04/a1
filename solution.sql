-- 1. Find all artists that has letter d in its name.
SELECT * FROM artists WHERE name LIKE "%d%";

-- 2. Find all songs that has a length of less than 3:50.
SELECT * FROM songs WHERE length < 350;

-- 3. Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.)
SELECT album_title, song_name, length FROM albums JOIN songs ON albums.id = songs.album_id;

-- 4. Join the 'artists' and 'albums' tables. (Find all albums that has letter a in its name.)
SELECT * FROM artists JOIN albums ON artists.id=albums.artist_id WHERE album_title LIKE "%a%";

-- 5. Sort the albums in Z-A order. (Show only the first 4 records.)
SELECT *  FROM albums ORDER BY album_title DESC LIMIT 4;

-- 6. Join the 'albums' and 'songs' tables. (Sort albums from Z-A)
SELECT *  FROM albums JOIN songs ON albums.id = songs.album_id ORDER BY album_title DESC, song_name ASC;